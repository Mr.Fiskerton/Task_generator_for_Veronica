#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPixmap>
#include "cmath"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QPixmap cat("cat.png");// /home/mrfiskerton/Documents/QtProjects/Task_generator_for_Veronica/
    int w = ui->label_pixImage->width(),
        h = ui->label_pixImage->height();
    ui->label_pixImage->setPixmap(cat.scaled(w, h, Qt::KeepAspectRatio));

    QRegExp regExp_degree("\\-?(([1-2]?\\d{2})|(3[0-5]?\\d?))");
    ui->lineEdit_condition_1->setValidator(new QRegExpValidator(regExp_degree, this));
    ui->lineEdit_condition_4->setValidator(new QRegExpValidator(regExp_degree, this));
    ui->lineEdit_condition_5->setValidator(new QRegExpValidator(regExp_degree, this));

    QRegExp regExp_digit("\\-?[1-9]{1}\\d{5}");
    ui->lineEdit_condition_2->setValidator(new QRegExpValidator(regExp_digit, this));
    ui->lineEdit_condition_3->setValidator(new QRegExpValidator(regExp_digit, this));
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_pushButton_randomConditions_clicked() {
    ui->lineEdit_condition_1->setText(QString::number(rand() % 360));
    ui->lineEdit_condition_2->setText(QString::number(rand() % 1000));
    ui->lineEdit_condition_3->setText(QString::number(rand() % 1000));
    ui->lineEdit_condition_4->setText(QString::number(rand() % 91 * (rand()%2 ? 1 : -1)));
    ui->lineEdit_condition_5->setText(QString::number(rand() % 26 * (rand()%2 ? 1 : -1)));

    ui->label_answer_1->setText(QString("..."));
    ui->label_answer_2->setText(QString("..."));
    ui->label_answer_3->setText(QString("..."));
    ui->label_answer_4->setText(QString("..."));
    ui->label_answer_5->setText(QString("..."));
}

template <typename T> int sign(T val) {
    return (T(0) < val) - (val < T(0));
}

void MainWindow::on_pushButton_showAnswers_clicked() {
    #define PI 3.14159265

    int MK = (ui->lineEdit_condition_1->text()).toInt();
    int V = (ui->lineEdit_condition_2->text()).toInt();
    int W = (ui->lineEdit_condition_3->text()).toInt();
    int YC = (ui->lineEdit_condition_4->text()).toInt();
    int dM = (ui->lineEdit_condition_5->text()).toInt();

    int U_eqv = W - V;

    int alpha = atan(V * sin(YC * PI/180) / U_eqv) * 180 / PI;
    if(sign(alpha) != sign(YC)) { alpha *= -1;}

    int U = V * sin(YC * PI/180) / sin(alpha * PI/180);//Warning: division by zero.

    int delta_n = ((W > V) ? (MK + YC + alpha + 360) % 360 : (MK + YC - alpha + 360 + 180) % 360);

    int delta = (delta_n + dM + 180) % 360;

    ui->label_answer_1->setText(QString::number(U_eqv));
    ui->label_answer_2->setText(QString::number(alpha));
    ui->label_answer_3->setText(QString::number(U));
    ui->label_answer_4->setText(QString::number(delta_n));
    ui->label_answer_5->setText(QString::number(delta));
}
